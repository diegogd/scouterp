# -*- coding: utf-8 -*-
{
    'name': 'Scout base',
    'version': '0.1',
    'category': 'Scout application',
    'summary': 'Scout Activities, Games, Songs',
    'description': """
Scout base
==========

Model an base data for scouting modeling.
""",
    'author': 'Diego Gómez (diegogd@gmail.com)',
    'depends': ["base", "portal", "report_aeroo", "report_aeroo_printscreen", "report_aeroo_ooo", "scout_template" ],
    'data': [ "data/scout_base_data.xml", "scout_unit_view.xml" ],
    'website': "http://scoutmanagement.somostrotamundos.es",
    'demo': [],
    'installable': True,
    'application': True,
    'images': [],
}
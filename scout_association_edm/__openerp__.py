# -*- coding: utf-8 -*-
{
    'name': 'Scout Association export to EDM',
    'version': '0.1',
    'category': 'Scout application',
    'summary': 'Let exporting Scout members to EDM members file',
    'description': """
Scout association export to EDM
===============================

Let exporting Scout members to EDM members file
""",
    'author': 'Diego Gómez (diegogd@gmail.com)',
    'depends': ["scout_association", "scouters_management"],
    'data': [
        'wizard/scout_association_members_edm_view.xml',
        'data/edm_reports.xml',
        'data/scout_base_edm_data.xml',
        'scout_members_edm_view.xml'
    ],
    'website': "http://scoutmanagement.somostrotamundos.es",
    'demo': [],
    'installable': True,
    'application': True,
    'images': [],
}
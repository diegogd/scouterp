import openerp
from osv import osv, fields
from openerp import tools
from datetime import date
from time import strptime
from openerp.tools.translate import _
import math


class scout_subscription_period_payment_wizard(osv.osv_memory):
    """
    Scout Subscription
    """
    _name = 'scout.subscription.period.wizard'
    _columns = {
        'period_id': fields.many2one('scout.subscription.period', 'Period'),
        'product_id': fields.many2one('product.product', 'Product'),
        'payment_day': fields.integer('Payment day')
    }
    _defaults = {
        'payment_day': 1
    }

    def generate(self,cr,uid,ids,context=None):
        ar_months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

        period_obj = self.pool.get('scout.subscription.period');
        payment_obj = self.pool.get('scout.subscription.period.payment');
        for wiz in self.browse(cr,uid,ids):
            if wiz.payment_day <= 0 or wiz.payment_day > 30:
                raise osv.except_osv('UserError','Please select a day between 1 and 30')
            if not wiz.period_id:
                raise osv.except_osv('UserError','Set Period')

            period = wiz.period_id;

            if period:
                starting_date = strptime(period.starting_date, "%Y-%m-%d");
                ending_date = strptime(period.ending_date, "%Y-%m-%d");

                start_year = starting_date[0];
                end_year = ending_date[0];

                start_month = starting_date[1];
                end_month = ending_date[1];

                total_months = (end_year - start_year)*12 + end_month - start_month;

                for i in range(0, total_months):
                    months_sum = (start_month+i);
                    month = ((months_sum-1) % 12) + 1;
                    year = start_year + math.floor(months_sum / 12);

                    concept = "Pago de cuota %s %d" %(_(ar_months[month-1]), year)

                    payment_obj.create(cr, uid, {'period_id': period.id, 'year': year, 'month': month, 'concept': concept, 'product_id': wiz.product_id.id });
            else:
                raise osv.except_osv('UserError', 'No period! '+period.id)
        return {}

scout_subscription_period_payment_wizard();
import openerp
from osv import osv, fields
from openerp import tools
from datetime import date
from time import strptime


class scout_subscription_period(osv.Model):
    """
    Scout Subscription
    """
    _name = 'scout.subscription.period'
    _description = 'Set a subscription period'
    _columns = {
        'name': fields.char('Name', size=200, readonly=True, states={'draft': [('readonly', False)]}),
        'starting_date': fields.date('Starting date', readonly=True, states={'draft': [('readonly', False)]}),
        'ending_date': fields.date('Ending date', readonly=True, states={'draft': [('readonly', False)]}),
        'description': fields.text('Period description', readonly=True, states={'draft': [('readonly', False)]}),
        'payment_ids': fields.one2many('scout.subscription.period.payment', 'period_id', 'Payments', readonly=True, states={'draft': [('readonly', False)]}),
        'state': fields.selection([
            ('draft', 'Draft'),
            ('active', 'Active'),
            ('inactive', 'Inactive')],
            'Status' )
    }
    _defaults = {
        'state': 'draft'
    }

    def action_generate_payments(self, cr, uid, ids, context=None):
        period_id = ids[0];
        wizard_id = self.pool.get('scout.subscription.period.wizard').create(cr, uid, {'period_id': period_id});
        return {
            'type': 'ir.actions.act_window',
            'name': 'Payment generation',
            'res_id': wizard_id,
            'res_model': 'scout.subscription.period.wizard',
            'view_type': 'form',
            'view_mode': 'form,tree',
            'view_id': False,
            'target': 'new'
        }

    def activity_active(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'active'}, context=context)
        return True

    def activity_inactive(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'inactive'}, context=context)
        return True

    def test_can_active(self, cr, uid, ids, *args):
        # raise openerp.exceptions.Warning(_('This activity programming is not complete, make sure you have filled the objectives, description, activities and the new for the web.'))
        # TODO: Completar con validad que compruebe que la activacion se hace dentro de las fechas correctas.
        return True

    def test_can_inactive(self, cr, uid, ids, *args):
        # raise openerp.exceptions.Warning(_('This activity programming is not complete, make sure you have filled the objectives, description, activities and the new for the web.'))
        # TODO: Completar con validad que compruebe que la activacion se hace dentro de las fechas correctas.
        return True



class scout_subscription_period_payment(osv.Model):
    """
    Scout Subscription
    """
    _name = 'scout.subscription.period.payment'
    _description = 'Period payment'
    _order = 'year, month'
    _rec_name = 'concept'
    _columns = {
        'period_id': fields.many2one('scout.subscription.period', 'Period'),
        'month': fields.selection([
            (1, 'January'),
            (2, 'February'),
            (3, 'March'),
            (4, 'April'),
            (5, 'May'),
            (6, 'June'),
            (7, 'July'),
            (8, 'August'),
            (9, 'September'),
            (10, 'October'),
            (11, 'November'),
            (12, 'December'),], 'Month'),
        'year': fields.integer('Year'),
        'concept': fields.char('Concept', size=256),
        'product_id': fields.many2one('product.product', 'Product')
    }


class scout_participant(osv.Model):
    """
    Scout Activity Objectives
    """
    _name = 'scout.participant'
    _description = 'Scout Participant'
    _inherit = 'scout.participant'

    def generate_payments(self, cr, uid, id, context=None):
        subscription_period_obj = self.pool.get('scout.subscription.period')
        participant_payment_obj = self.pool.get('scout.participant.payment')
        invoice_obj = self.pool.get('account.invoice')
        product_obj = self.pool.get('product.product')

        for participant in self.browse(cr, uid, id, context):

            if not participant.family_id:
                raise openerp.exceptions.Warning(_('The participant is not complete. Please configure a family for this participant.'))

            participant_payments_ids = map(lambda payment: payment.period_payment_id.id, participant.payment_ids)
            active_periods = subscription_period_obj.search(cr, uid, [('state', '=', 'active')])
            partner = participant.family_id.partner_id

            account_id = partner.property_account_receivable and partner.property_account_receivable.id or False
            fpos_id = partner.property_account_position and partner.property_account_position.id or False

            for period in subscription_period_obj.browse(cr, uid, active_periods, context):
                for payment in period.payment_ids:
                    if payment.id not in participant_payments_ids:
                        invoice_id = invoice_obj.create(cr, uid, {'partner_id': partner.id, 'account_id': account_id, 'fiscal_position': fpos_id or False})
                        participant_payment_obj.create(cr, uid,
                                                       {'participant_id': participant.id,
                                                        'period_payment_id': payment.id,
                                                        'invoice_id': invoice_id})


        return True

    _columns = {
        'payment_ids': fields.one2many('scout.participant.payment', 'participant_id', 'Payments'),
    }


class scout_participant_payment(osv.Model):
    """
    Scout Participant payment
    """
    _name = 'scout.participant.payment'
    _description = 'Payment with invoice of a participant subscription'
    _columns = {
        'participant_id': fields.many2one('scout.participant', 'Participant'),
        'period_payment_id': fields.many2one('scout.subscription.period.payment', 'Period payment'),
        'invoice_id': fields.many2one('account.invoice', 'Invoice')
    }
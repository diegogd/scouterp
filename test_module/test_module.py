from osv import osv,fields


class test_base(osv.osv):
    """
    Test Base Class
    """
    _name = 'test.base'
    _columns = {
        'name': fields.char("Name", size=128,),
        'lastname': fields.char("Last Name", size=128,),
        'birthday': fields.date("Bithday"),
        'nif': fields.char('DNI/NIF', size=20),
        'code': fields.char("Code", size=64,)
    }




test_base()
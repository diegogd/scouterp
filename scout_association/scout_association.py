import openerp
from osv import osv, fields
from openerp import tools
from datetime import date
from time import strptime
import base64
import mimetypes
from tools.translate import _

import logging

_logger = logging.getLogger(__name__)

class scout_member(osv.Model):
    """
    Scout Activity Objectives
    """
    _name = 'scout.member'
    _description = 'Scout Member'

    def _has_image(self, cr, uid, ids, name, args, context=None):
        result = {}
        for obj in self.browse(cr, uid, ids, context=context):
            result[obj.id] = obj.image != False
        return result

    def _has_image(self, cr, uid, ids, name, args, context=None):
        result = {}
        for obj in self.browse(cr, uid, ids, context=context):
            result[obj.id] = obj.image != False
        return result

    def _get_current_age(self, cr, uid, ids, name, args, context=None):
        result = {}
        for obj in self.browse(cr, uid, ids, context=context):
            age = 0

            if obj.birthday:
                birthday_date = strptime(obj.birthday, "%Y-%m-%d")
                delta = date.today() - date(birthday_date[0], birthday_date[1], birthday_date[2])
                age = delta.days / 365.25

            result[obj.id] = age
        return result

    _columns = {
        'name': fields.char("Name", size=150,),
        'last_name': fields.char("Name", size=150,),
        'birthday': fields.date('Birthday'),
        'age': fields.function(_get_current_age, string="Age", type="integer"),
        'image': fields.binary("Image",
            help="This field holds the image used as avatar for this contact, limited to 1024x1024px"),
        'has_image': fields.function(_has_image, type="boolean"),
        'gender': fields.selection([('m','Male'),('f','Female'),('o','Other')], string='Gender', required=False),
        'nationality': fields.many2one('res.country', string='Nationality'),
        'dni': fields.char("DNI", size=15,)
    }


class scout_parent(osv.Model):
    """
    Scout Activity Objectives
    """
    _name = 'scout.parent'
    _description = 'Scout Parent'
    _inherit = 'scout.member'
    _order = 'sequence'
    def _get_image(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, False)
        for obj in self.browse(cr, uid, ids, context=context):
            result[obj.id] = tools.image_get_resized_images(obj.image)
        return result

    def _set_image(self, cr, uid, id, name, value, args, context=None):
        return self.write(cr, uid, [id], {'image': tools.image_resize_image_big(value)}, context=context)
    _columns = {
        'family_id': fields.many2one('scout.family', 'Family', ondelete='cascade'),
        'sequence': fields.integer('Contact Preferent Order'),
        'email': fields.char("E-mail", size=200),
        'phone': fields.char("Phone", size=200),
        'phone-alt': fields.char("Alternative Phone", size=200),
        'preference_order': fields.integer("Preference order"),
        'profession': fields.char("Profession", size=255),
        'relationship_id': fields.many2one('scout.parent.relationship', 'Relationship'),
        'details': fields.text('Details'),
        'image_medium': fields.function(_get_image, fnct_inv=_set_image,
            string="Medium-sized image", type="binary", multi="_get_image",
            store={
                _name: (lambda self, cr, uid, ids, c={}: ids, ['image'], 10),
            },
            help="Medium-sized image of this contact. It is automatically resized as a 128x128px image, with aspect ratio preserved. Use this field in form views or some kanban views."),
        'image_small': fields.function(_get_image, fnct_inv=_set_image,
            string="Small-sized image", type="binary", multi="_get_image",
            store={
                _name: (lambda self, cr, uid, ids, c={}: ids, ['image'], 10),
            },
            help="Small-sized image of this contact. It is automatically resized as a 64x64px image, with aspect ratio preserved. Use this field anywhere a small image is required."),
    }


class scout_participant(osv.Model):
    """
    Scout Activity Objectives
    """
    _name = 'scout.participant'
    _description = 'Scout Participant'
    _inherit = 'scout.member'
    def _get_image(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, False)
        for obj in self.browse(cr, uid, ids, context=context):
            result[obj.id] = tools.image_get_resized_images(obj.image)
        return result

    def _set_image(self, cr, uid, id, name, value, args, context=None):
        return self.write(cr, uid, [id], {'image': tools.image_resize_image_big(value)}, context=context)

    def _get_has_authorization(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, False)
        for obj in self.browse(cr, uid, ids, context=context):
            item = obj.member_authorization
            result[obj.id] = (item is not False)
        return result

    def _get_guest_filename(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, False)
        for obj in self.browse(cr, uid, ids, context=context):
            result[obj.id] = _('authorization-%s.pdf') % ("(" + obj.name + "-" + obj.member_number + ")")
        return result

    _order = 'birthday desc'

    _columns = {
        'nickname': fields.char("Nickname", size=150,),
        'member_number': fields.char("Member number", size=40,),
        'email': fields.char("E-mail", size=200),
        'family_id': fields.many2one('scout.family', 'Family', ondelete='cascade'),
        'unit_id': fields.many2one('scout.unit', 'Unit', ondelete='cascade'),
        'subunit_id': fields.many2one('scout.unit.subunit', 'Subunit', ondelete='cascade', domain="[('unit_id', '=', unit_id)]" ),
        'member_since': fields.date('Member since'),
        'member_authorization': fields.binary("Member authorization", filters="*.pdf"),
        'member_authorization_name': fields.function(_get_guest_filename,
            string="Member authorization filename", type="string"),
        'has_authorization': fields.function(_get_has_authorization, string="Has authorization", type="boolean"),
        'school_name': fields.char("School name", size=150,),
        'school_grade': fields.char("School grade", size=150,),
        'image_medium': fields.function(_get_image, fnct_inv=_set_image,
            string="Medium-sized image", type="binary", multi="_get_image",
            store={ _name: (lambda self, cr, uid, ids, c={}: ids, ['image'], 10) }),
        'image_small': fields.function(_get_image, fnct_inv=_set_image,
            string="Small-sized image", type="binary", multi="_get_image",
            store={ _name: (lambda self, cr, uid, ids, c={}: ids, ['image'], 10) }),
        'achievements_ids': fields.one2many('scout.participant.achievement', 'participant_id', 'Achievements'),
        'main_phone': fields.related('family_id', 'partner_id', 'phone', readonly=False, type='char', size=64, string='Main phone'),
        'parent_ids': fields.related('family_id', 'parent_ids', readonly=False, type='one2many',
                    relation='scout.parent', string='Parents information',),
        'state': fields.selection([
            ('preregistered', 'Preregistered'),
            ('subscribed', 'Subscribed'),
            ('unsubscribed', 'Unsubscribed')],
            'Status', readonly=True, track_visibility='onchange''Status')
    }
    _defaults = {
        'state': 'preregistered'
    }

    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        context = context or {}

        if 'list_unsubscribed_members' not in context.keys():
            args.append(('state', '!=', 'unsubscribed'))

        if 'list_my_unit' in context.keys():
            obj_scouter = self.pool.get('scout.scouter')
            scouter_ids = obj_scouter.search(cr, uid, [('user_id', '=', uid)])

            for scouter in obj_scouter.browse(cr, uid, scouter_ids, context=context):
                _logger.warn(args)
                args.append(('unit_id', '=', scouter.unit_id.id))

        return super(scout_participant,self).search(cr, uid, args, offset=offset, limit=limit, order=order, context=context, count=count)

    def action_subscribe(self, cr, uid, ids, context=None):
        self.write(cr,uid,ids,{'state':'subscribed'})
        return True

    def action_unsubscribe(self, cr, uid, ids, context=None):
        self.write(cr,uid,ids,{'state':'unsubscribed'})
        return True

class scout_family(osv.Model):
    """
    Scout Activity Objectives
    """
    _name = 'scout.family'
    _description = 'Group Family'

    def create(self, cr, uid, vals, context=None):
        obj_partner = self.pool.get('res.partner')
        partner_id = obj_partner.create(cr, uid, {'name': vals['name'], 'is_company':False}, context=context)
        vals.update({'partner_id': partner_id})
        family_id = super(scout_family, self).create(cr, uid, vals, context=context)
        return family_id

    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        context = context or {}

        if 'list_active_families' in context.keys():

            obj_participants = self.pool.get('scout.participant')
            participants_ids = obj_participants.search(cr, uid, [('state', 'in', ['subscribed', 'preregistered'])])

            families_ids = []
            for participant in obj_participants.browse(cr, uid, participants_ids, context=context):
                _logger.warn(args)
                families_ids.append(participant.family_id.id)

            args.append(('id', 'in', families_ids))

        return super(scout_family,self).search(cr, uid, args, offset=offset, limit=limit, order=order, context=context, count=count)

    _columns = {
        'name': fields.char("Name", size=100,),
        'partner_id': fields.many2one('res.partner', 'Partner'),
        'parent_ids': fields.one2many('scout.parent', 'family_id', 'Parents'),
        'child_ids': fields.one2many('scout.participant', 'family_id', 'Participants'),
        'communication_ids': fields.one2many('scout.family.communication', 'family_id', 'Communications'),
        'street': fields.related('partner_id', 'street',
                  readonly=False, type='char', size=128, string='Street'),
        'street2': fields.related('partner_id', 'street2',
                    readonly=False, type='char', size=128, string='Street 2'),
        'zip': fields.related('partner_id', 'zip',
               readonly=False, type='char', size=24, string='zip'),
        'city': fields.related('partner_id', 'city', readonly=False, type='char', size=128, string='City'),
        'state_id': fields.related('partner_id','state_id', readonly=False,
                    type='many2one', domain="[('country_id', '=', country_id)]", relation='res.country.state',
                    string='State'),
        'payment_method_id': fields.many2one('scout.family.payment_method', 'Payment method', ondelete='cascade' ),
        'bank_ids': fields.related('partner_id','bank_ids',
                    readonly=False, type='one2many',
                    relation='res.partner.bank', string='Bank accounts', domain="[('partner_id', '=', partner_id)]"),
        'country_id': fields.related('partner_id', 'country_id', readonly=False, type='many2one', relation='res.country', string='Country'),
        'email': fields.related('partner_id', 'email', readonly=False, type='char', size=240, string='Email'),
        'phone': fields.related('partner_id', 'phone', readonly=False, type='char', size=64, string='Phone'),
    }

class scout_family_payment_method(osv.Model):
    """
    Scout achievements
    """
    _name = 'scout.family.payment_method'
    _description = 'Family payment method'
    _columns = {
        'name': fields.char("Name", size=100),
        'description': fields.text("Description")
    }

class scout_family_communication(osv.Model):
    """
    Scout Activity Objectives
    """
    _name = 'scout.family.communication'
    _description = 'Group Family Communication'
    _rec_name = 'title'
    _order = 'communication_date desc'
    _columns = {
        'title': fields.char("Title", size=255,),
        'family_id': fields.many2one('scout.family', 'Family'),
        'description': fields.text("Description"),
        'communication_date': fields.datetime("Communication date"),
        'communication_type': fields.selection([
            ('email', 'E-mail'),
            ('web', 'Web'),
            ('phone', 'Phone'),
            ('conversation', 'Conversation'),
            ('other', 'Other')],
            'Communication type')
    }

class scout_parent_relationship(osv.Model):
    """
    Scout Activity Objectives
    """
    _name = 'scout.parent.relationship'
    _description = 'Parent relationship'
    _columns = {
        'name': fields.char("Name", size=255,)
    }


class scout_participant_achievement(osv.Model):
    """
    Scout achievements
    """
    _name = 'scout.participant.achievement'
    _description = 'Scout participant achievement'
    _columns = {
        'participant_id': fields.many2one('scout.participant', 'Participant'),
        'achievement_id': fields.many2one('scout.achievement', 'Achievement' ),
        'achieved_in': fields.date('Achieved in'),
        'description': fields.text("Description")
    }


class scout_achievement(osv.Model):
    """
    Scout achievements
    """
    _name = 'scout.achievement'
    _description = 'Scout achievement'
    _columns = {
        'name': fields.char("Name", size=100),
        'description': fields.text("Description"),
        'ages_ids': fields.many2many('scout.age', 'scout_achievement_age_rel', 'achievement_id', 'age_id', 'Achievement for ages'),
        'image': fields.binary("Image", help="This field holds the image used as icon for the achievement 1024x1024px"),
    }
import openerp
from osv import osv, fields

class scout_participant(osv.Model):
    """
    Scout Activity Objectives
    """
    _name = 'scout.participant'
    _description = 'Scout Participant'
    _inherit = 'scout.participant'

    _columns = {
        'objectives_ids': fields.one2many('scout.participant.objective', 'participant_id', 'Objectives'),
    }

class scout_participant_objective(osv.Model):
    """
    Scout participant evaluation
    """
    _name = 'scout.participant.objective'
    _description = 'Scout Participant objective'

    _columns = {
        'participant_id': fields.many2one('scout.participant', 'Participant', ondelete='cascade'),
        'objective_date': fields.datetime("Objective date"),
        'authors_ids': fields.many2many('scout.scouter', 'scout_scouter_rel', 'evaluation_id', 'scouter_id', 'Reviewed by scouters'),
        'behavioral_observations': fields.text('Behavioral observations'),
        'objectives': fields.text('Objectives'),
        'guidelines_for_group': fields.text('Guidelines for group'),
        'guidelines_for_home': fields.text('Guidelines for home'),
        'reviews_of_progress_ids': fields.one2many('scout.participant.objective.review', 'objective_id', 'Reviews of progress'),
    }


class scout_participant_objective_review(osv.Model):
    """
    Scout participant evaluation
    """
    _name = 'scout.participant.objective.review'
    _description = 'Scout Participant objective review'

    _columns = {
        'objective_id': fields.many2one('scout.participant.objective', 'Participant objective', ondelete='cascade'),
        'review_date': fields.datetime("Review date"),
        'authors_ids': fields.many2many('scout.scouter', 'scout_scouter_rel', 'evaluation_id', 'scouter_id', 'Reviewed by scouters'),
        'comment': fields.text('Comment'),
    }
# -*- coding: utf-8 -*-
{
    'name': 'Scout template',
    'version': '0.1',
    'category': 'Scout application',
    'summary': 'Scout Activities, Games, Songs',
    'description': """
Scout template
==============

Custom login template for openERP
""",
    'author': 'Diego Gómez (diegogd@gmail.com)',
    'depends': ['web'],
    'data': [],
    'website': "http://scoutmanagement.somostrotamundos.es",
    'demo': [],
    'installable': True,
    'auto_install': True,
    'images': [],
    'qweb': ['static/src/xml/base.xml'],
    'css': ['static/src/css/base.css'],
}
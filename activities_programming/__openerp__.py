# -*- coding: utf-8 -*-
{
    'name': 'Activities Programming',
    'version': '0.1',
    'category': 'Scout application',
    'summary': 'Scout Activities, Games, Songs',
    'description': """
Activities Programming
======================

This module has been created for managing different kinds of activities like games, songs
arts & crafts, etc.
""",
    'author': 'Diego Gómez (diegogd@gmail.com)',
    'depends': ['web', "base", "scout_base", "activities_management", "wordpress_publication"],
    'data': [
        "activities_programming_evaluation_view.xml",
        "activities_programming_attendance_view.xml",
        "activities_programming_view.xml",
        "activities_programming_workflow.xml",
        "wizard/scout_evaluation_select_template_wizard_view.xml",
        "annual_plan_view.xml",
        "annual_plan_workflow.xml",
        "security/ir.model.access.csv"
    ],
    'website': "http://scoutmanagement.somostrotamundos.es",
    'demo': [],
    'installable': True,
    'application': True,
    'images': [],
    'css': ["static/src/css/styles_programming.css"],
}
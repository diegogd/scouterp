from report import report_sxw
from report.report_sxw import rml_parse
import html2text

class Parser(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(Parser, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'html2text': self.parse_html2text,
            'html': self.parse_html2text,
        })

    def parse_html2text(self, content):

        h = html2text.HTML2Text()
        h.ignore_links = True
        h.ignore_images = True
        h.ignore_emphasis = True

        parsed = h.handle(content)
        return parsed

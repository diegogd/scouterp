# -*- coding: utf-8 -*-
# "l10n_es_partner", "l10n_es_toponyms", "l10n_es_account_asset", "account_voucher", "hr_expense"
{
    'name': 'Wordpress publication',
    'version': '0.1',
    'category': 'Scout application',
    'summary': 'Wordpress posts writer',
    'description': """
Wordpress publication
=====================

This tool allow openerp to publish posts on any instance of wordpress, assign some tags and parameters from
a model object.
""",
    'author': 'Diego Gómez (diegogd@gmail.com)',
    'depends' : ['base','base_setup'],
    'data': [
        "wizard/wordpress_configuration_view.xml",
        "security/ir.model.access.csv"
    ],
    'website': "http://scoutmanagement.somostrotamundos.es",
    'demo': [],
    'installable': True,
    'application': True,
    'images': [],
}
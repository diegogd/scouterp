from datetime import datetime
from osv import osv,fields
from wordpress_xmlrpc import Client, WordPressPost
from wordpress_xmlrpc.methods.posts import GetPosts, GetPost, NewPost, EditPost
from wordpress_xmlrpc.methods.users import GetUserInfo


class wordpress_post(osv.Model):
    """
    Scout Activity Objectives
    """
    _name = 'wordpress.post'
    _description = 'Wordpress post'

    def publishPost(self, cr, uid, ids, context=None):
        record = self.browse(cr, uid, ids[0], context)

        endpoint = self.pool.get("ir.config_parameter").get_param(cr, uid, "wordpress.xmlrpc.host")
        user = self.pool.get('ir.config_parameter').get_param(cr, uid, 'wordpress.username')
        password = self.pool.get('ir.config_parameter').get_param(cr, uid, 'wordpress.password')

        wp = Client(endpoint, user, password)

        post = WordPressPost()
        post.title = record.title
        post.content = record.content
        if record.publication_date:
            post.date = datetime.strptime(record.publication_date, "%Y-%m-%d %H:%M:%S")
        #post.date = datetime.strftime('%Y-%m-%d', )

        #print post.date
        # post.date =
        post.post_status = 'publish' # 'draft'

        if record.post_tag and record.post_tag:
            post_tags = record.post_tag.split(', ')
        else:
            post_tags = []

        if record.category and len(record.category):
            post_categories = record.category.split(', ')
        else:
            post_categories = []

        post.terms_names = {
            'post_tag': post_tags,
            'category': post_categories
        }

        if record.post_id:
            post.id = record.post_id
            wp.call(EditPost(post.id, post))
        else:
            post.id = wp.call(NewPost(post))

        post = wp.call(GetPost(post.id))
        self.write(cr, uid, ids, {'post_id': post.id, 'post_url': post.link, 'publication_date': post.date.strftime("%Y-%m-%d %H:%M:%S")}, context=context)

    _columns = {
        'title': fields.char("Title", size=256,),
        'content': fields.text("Content"),
        'category': fields.char("Category", size=256,),
        'post_tag': fields.char("Post tag", size=256,),
        'publication_date': fields.datetime('Publication date'),
        'post_id': fields.integer("PostID"),
        'post_url': fields.char("Post url", size=256),
    }

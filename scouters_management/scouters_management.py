import openerp
from osv import osv, fields
from openerp import tools
from datetime import date
from time import strptime

class scout_scouter(osv.Model):
    """
    Scout Activity Objectives
    """
    _name = 'scout.scouter'
    _description = 'Scout Scouter'
    _inherit = 'scout.member'

    def create(self, cr, uid, vals, context=None):
        obj_partner = self.pool.get('res.partner')
        obj_user = self.pool.get('res.users')
        user_id = obj_user.create(cr, uid, {'name': vals['name'], 'login': vals['login']}, context=context)
        vals.update({'user_id': user_id})
        partner_id = obj_partner.create(cr, uid, {'name': vals['name'], 'is_company': False, 'user_id': user_id}, context=context)
        vals.update({'partner_id': partner_id, 'user_id': user_id})

        scouter_id = super(scout_scouter, self).create(cr, uid, vals, context=context)
        return scouter_id

    def _get_image(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, False)
        for obj in self.browse(cr, uid, ids, context=context):
            result[obj.id] = tools.image_get_resized_images(obj.image)
        return result

    def _set_image(self, cr, uid, id, name, value, args, context=None):
        return self.write(cr, uid, [id], {'image': tools.image_resize_image_big(value)}, context=context)

    _columns = {
        'nickname': fields.char("Nickname", size=150,),
        'member_number': fields.char("Member number", size=40,),
        # Partner fields
        'login': fields.related('user_id', 'login', readonly=False, type='char', size=64, string='Login'),
        'user_id': fields.many2one('res.users', 'User'),
        'partner_id': fields.many2one('res.partner', 'Partner'),
        'unit_id': fields.many2one('scout.unit', 'Scouting in unit', ondelete='cascade'),
        'street': fields.related('partner_id', 'street', readonly=False, type='char', size=128, string='Street'),
        'street2': fields.related('partner_id', 'street2',
                    readonly=False, type='char', size=128, string='Street 2'),
        'zip': fields.related('partner_id', 'zip',
               readonly=False, type='char', size=24, string='zip'),
        'city': fields.related('partner_id', 'city', readonly=False, type='char', size=128, string='City'),
        'state_id': fields.related('partner_id','state_id', readonly=False,
                    type='many2one', domain="[('country_id', '=', country_id)]", relation='res.country.state',
                    string='State'),
        'bank_ids': fields.related('partner_id','bank_ids',
                    readonly=False, type='one2many',
                    relation='res.partner.bank', string='Bank accounts', domain="[('partner_id', '=', partner_id)]"),
        'country_id': fields.related('partner_id', 'country_id', readonly=False, type='many2one', relation='res.country', string='Country'),
        'email': fields.related('partner_id', 'email', readonly=False, type='char', size=240, string='Email'),
        'phone': fields.related('partner_id', 'phone', readonly=False, type='char', size=64, string='Phone'),
        'mobile': fields.related('partner_id', 'mobile', readonly=False, type='char', size=64, string='Mobile'),

        'member_since': fields.date('Member since'),

        'image': fields.related('user_id', 'image', readonly=False, type='binary', string='Image'),
        'image_medium': fields.function(_get_image, fnct_inv=_set_image,
            string="Medium-sized image", type="binary", multi="_get_image",
            store={ _name: (lambda self, cr, uid, ids, c={}: ids, ['image'], 10) }),
        'image_small': fields.function(_get_image, fnct_inv=_set_image,
            string="Small-sized image", type="binary", multi="_get_image",
            store={ _name: (lambda self, cr, uid, ids, c={}: ids, ['image'], 10) }),

        'achievements_ids': fields.one2many('scout.scouter.achievement', 'scouter_id', 'Achievements'),
        'trainings_ids': fields.one2many('scout.scouter.training', 'scouter_id', 'Trainings'),

        'state': fields.selection([
            ('active', 'Active'),
            ('support', 'Support'),
            ('inactive', 'inactive')],
            'Status', readonly=False)
    }
    _defaults = {
        'state': 'active'
    }


class scout_scouter_achievement(osv.Model):
    """
    Scout achievements
    """
    _name = 'scout.scouter.achievement'
    _description = 'Scout participant achievement'
    _columns = {
        'scouter_id': fields.many2one('scout.scouter', 'Scouter'),
        'achievement_id': fields.many2one('scout.achievement', 'Achievement' ),
        'achieved_in': fields.date('Achieved in'),
        'description': fields.text("Description")
    }


class scout_scouter_training(osv.Model):
    """
    Scout achievements
    """
    _name = 'scout.scouter.training'
    _description = 'Scout participant training'
    _columns = {
        'scouter_id': fields.many2one('scout.scouter', 'Scouter'),
        'training_id': fields.many2one('scout.training', 'Training'),
        'achieved_in': fields.date('Achieved in'),
        'attachment_ids': fields.many2many('ir.attachment', 'scouter_training_attachments', 'scoute_scouter_training_id', 'attachment_id', 'Attachments'),
        'description': fields.text("Description")
    }


class scout_achievement(osv.Model):
    """
    Scout achievements
    """
    _name = 'scout.training'
    _description = 'Scouters general training'
    _columns = {
        'name': fields.char("Name", size=100),
        'description': fields.text("Description")
    }
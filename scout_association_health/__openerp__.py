# -*- coding: utf-8 -*-
{
    'name': 'Scout Association Health',
    'version': '0.1',
    'category': 'Scout application',
    'summary': 'Scout members, parents, scouters, evaluation of participants',
    'description': """
Scout Association Health information
====================================

Manage scout participants health information.
""",
    'author': 'Diego Gómez (diegogd@gmail.com)',
    'depends': ["scout_association"],
    'data': [
        "scout_participant_view.xml",
        "security/scout_security.xml",
    ],
    'website': "http://scoutmanagement.somostrotamundos.es",
    'demo': [  ],
    'installable': True,
    'application': True,
    'images': [],
}